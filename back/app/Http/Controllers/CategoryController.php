<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateCategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        return Category::all();
    }

    public function destroy(Request $request,Category $category)
    {
        if ($category->id == 1)
            return response('شما نمیتوانید دسته بندی پیش فرض را حذف کنید ');
        $category->products()->update(
            ['category_id' => Category::query()
                ->where('name', 'دسته بندی نشده')
                ->first()->id]
        );

        $category->delete();

        return response('با موفقیت انجام شد');
    }

    public function update(UpdateCategoryRequest $request,Category $category)
    {
            $category->update($request->validated());

    }

    public function store (UpdateCategoryRequest $request)
    {
        Category::create($request->validated());
    }
}
