<?php

namespace App\Http\Controllers;

use App\Http\Requests\BuyRequest;
use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Product;
use App\Models\Transaction;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function store(CreateProductRequest $request)
    {
        $data = $request->validated();
        $data['sold_number'] = 0;
        $data['quantity_number'] = 0;
        $data['image_path'] = 'https://fiverr-res.cloudinary.com/images/q_auto,f_auto/gigs/119193411/original/d1aa83e26316e9b0b45856a958687fe74978a688/shoot-professional-product-photography-on-white-background.png';

        Product::create($data);
    }

    public function index(Request $request)
    {
        $products = Product::query();

        if ($request->has('filters')) {
            $categories = json_decode($request->filters)->categories ?? null;
            if ($categories)
                $products->whereIn('category_id', $categories);
        }
        if ($request->has('sort'))
            $products->orderBy($request->sort,$request->order);

        if ($request->has('search'))
            $products->where('name','LIKE' , "%{$request->search}%");

        return $products->with('category')->paginate($request->perPage ?? 3);
    }

    public function update(UpdateProductRequest $request, Product $product)
    {
        $product->update($request->validated());
    }

    public function destroy(Product $product)
    {
        $product->delete();
    }

    public function buy(BuyRequest $request,Product $product)
    {
        if ($request->product_number > $product->quantity_number) {
            return response('موجودی نداریم !');
        }
        $totalPrice = $product->price * $request->product_number;
        if (auth()->user()->balance < $totalPrice)
            return response('موجودی نداری !');

        Transaction::create([
            'product_name'=> $product->name,
            'quantity'=> $request->product_number,
            'price'=> $totalPrice,
            "buyer_address" => auth()->user()->address,
            "buyer_full_name" => auth()->user()->first_name . ' '. auth()->user()->last_name,
            "buyer_id" => auth()->user()->id,
            "product_id" => $product->id
        ]);

        auth()->user()->update(['balance'=> auth()->user()->balance - $totalPrice]);
        $product->update(['sold_number' => $product->sold_number +1]);
        $product->update(['quantity_number' =>$product->quantity_number  -1]);

        return  response('با موفقیت خریدیش !');
    }
}
