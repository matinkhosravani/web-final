<?php

namespace App\Http\Controllers;

use App\Http\Requests\updateProfileRequest;
use App\Models\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function update(updateProfileRequest $request)
    {
        $user = User::query()
            ->where('id',auth()->user()->id)
            ->update($request->validated());

        return response('با موفقیت انجام شد!');

    }
}
