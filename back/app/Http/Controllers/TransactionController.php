<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function index()
    {
        if (auth()->user()->isAdmin())
            return Transaction::all();
        else
            return auth()->user()->transactions;
    }

    public function search(Request $request)
    {
        return Transaction::query()
            ->where('id','LIKE' , "%{$request->s}%")
            ->get();

    }
}
