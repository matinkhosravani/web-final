<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function deposit(Request $request)
    {
        User::query()->where('id',auth()->user()->id)
            ->increment('balance', 50000);

        return response('با موفقیت انجام شد ');
    }
}
