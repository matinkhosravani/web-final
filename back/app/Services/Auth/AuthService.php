<?php


namespace App\Services\Auth;


use Tymon\JWTAuth\JWTAuth;

class AuthService
{
    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createNewToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTl() * 60,
            'user' => auth()->user()
        ]);
    }
}
