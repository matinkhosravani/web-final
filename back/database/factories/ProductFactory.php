<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'price' => $this->faker->numberBetween(0,1000000),
            'sold_number' => $this->faker->numberBetween(0,100),
            'quantity_number' => $this->faker->numberBetween(0,100),
            'image_path' => $this->faker->filePath(),
        ];
    }
}
