<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('auth/login', [AuthController::class, 'login']);
Route::post('auth/register', [AuthController::class, 'register']);
Route::group(['middleware' => 'auth:api', 'prefix' => 'auth'], function () {
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::get('/user', [AuthController::class, 'userProfile']);
});


Route::group(['prefix' => 'categories'],function (){
   Route::get('/' , [CategoryController::class,'index']);
    Route::group(['middleware' => 'isAdmin'], function () {
        Route::delete('/{category}' , [CategoryController::class,'destroy']);
        Route::patch('/{category}' , [CategoryController::class,'update']);
        Route::post('/' , [CategoryController::class,'store']);
    });
});


Route::group(['prefix' => 'products'] ,function (){
    Route::get('/', [ProductController::class, 'index']);
    Route::group(['middleware' => 'isAdmin'], function () {
        Route::post('/', [ProductController::class, 'store']);
        Route::patch('/{product}', [ProductController::class, 'update']);
        Route::delete('/{product}', [ProductController::class, 'destroy']);
    });
    Route::post('/{product}/buy', [ProductController::class, 'buy']);
});

Route::group(['prefix' => 'transactions'], function () {
    Route::get('/', [TransactionController::class, 'index']);
    Route::get('/search', [TransactionController::class, 'search']);
});


Route::group(['middleware' => 'auth', 'prefix' => 'profile'], function () {
    Route::patch('/', [ProfileController::class, 'update']);
});

Route::group(['middleware' => 'auth', 'prefix' => 'users'], function () {
    Route::post('/deposit', [UserController::class, 'deposit']);
});
