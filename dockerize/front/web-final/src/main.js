import {createApp} from 'vue'
import App from './App.vue'
import {store} from "./store";
import axios from "axios";
import router from './router'

App.$http = axios;

const token = localStorage.getItem("token");
if (token) {
    App["$http"].defaults.headers.common["Authorization"] = `bearer ${token}`;
}

const app = createApp(App)
app.config.globalProperties.$backUrl = "http://web"
app.config.globalProperties.$backApiUrl = "http://web/api"
app.use(router)
app.use(store)
app.mount('#app')
