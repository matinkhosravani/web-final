import {createStore} from "vuex"

import axios from "axios";

export const store = new createStore({
    state: {
        status: "",
        token: localStorage.getItem("token") || "",
        user: {}
    },
    mutations: {
        auth_request(state) {
            state.status = "loading";
        },
        auth_success(state, token, user) {
            state.status = "success";
            state.token = token;
            state.user = user;
        },
        auth_error(state) {
            state.status = "error";
        },
        logout(state) {
            state.status = "";
            state.token = "";
        }
    },
    actions: {
        login({commit}, user) {
            return new Promise((resolve, reject) => {
                commit("auth_request");
                axios({
                    url: "http://web/api/auth/login",
                    data: user,
                    method: "POST"
                })
                    .then(resp => {
                        const token = resp.data.access_token;
                        const user = resp.data.user;
                        localStorage.setItem("token", token);
                        localStorage.setItem("user", JSON.stringify(user));
                        axios.defaults.headers.common["Authorization"] = token;
                        commit("auth_success", token, user);
                        resolve(resp);
                    })
                    .catch(err => {
                        commit("auth_error");
                        localStorage.removeItem("token");
                        reject(err);
                    });
            });
        },
        register({commit}, user) {
            return new Promise((resolve, reject) => {
                axios({
                    url: "http://web/api/auth/register",
                    data: user,
                    method: "POST"
                })
                    .then(resp => {
                        commit("auth_request");
                        const token = resp.data.access_token;
                        const user = resp.data.user;
                        localStorage.setItem("token", token);
                        localStorage.setItem("user", JSON.stringify(user));
                        axios.defaults.headers.common["Authorization"] = token;
                        commit("auth_success", token, user);
                        resolve(resp);
                    })
                    .catch(err => {
                        commit("auth_error", err);
                        localStorage.removeItem("token");
                        reject(err);
                    });
            });
        },
        logout({commit}) {
            return new Promise(resolve => {
                axios.post("http://web/api/auth/logout")
                    .then(function () {
                        localStorage.removeItem("token");
                        localStorage.removeItem("user");
                        delete axios.defaults.headers.common["Authorization"]
                    })
                    .catch(err => console.log(err))
                commit("logout");
                this.$router.push('/')
                resolve();
            });
        }
    },
    getters: {
        isLoggedIn: state => !!state.token,
        authStatus: state => state.status,
        user: state => state.user
    }
});
